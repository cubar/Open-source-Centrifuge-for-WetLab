Working with the centrifuge

The main screen shows three items:
- preset speed
  - Click the rotary encoder to select it
  - Turn the rotary encoder to select a speed between 0 and 52
  - Click the rotary encoder again to start the motor
  - Click again to stop
- calibration
  To calibrate, the procedure is:
  - choose calibrate from the menu, clicking the rotary encoder
  - switch on power to the motor
  - wait for the lower tone and click the rotary encoder again
  - calibration should be done, returning to main menu
- live speed
  - Turn the rotary encoder and click "live speed"
  - Turn the rotary encoder and the motor will start spinning when the speed is
    4 or more
  - Click the rotary encoder again to stop the motor

In the folder "src" you will find the following library files:
- menu.py     # Menu class for menu traversal
- motor.py    # Motor class for a brushless motor
- rotary.py   # Rotary class for a rotary encoder
- ssd1306.py  # ssd1306 class for an oled display
- switch.py   # Switch class with debouncing
- uftpd.py    # Ftp server for easy uploading (not working with esp8266)
- unix.py     # Some unix like utilities for use on the repl command line

There are standard micropython files:
- boot.py        # here we included activation of uftpd
- webrepl_cfg.py # the webrepl is also actitivated from boot.py

The main file:
- main.py # After booting, micropython executes main.py

Just upload all these scripts to your esp and you should be good to go.
Optionally you can make a subfolder called "lib" to store the above mentioned
library files. Micropython will find the files independent of this option. The
other files have to be in the root folder of the device.

In main.py you can adapt the conguration variable "cfg" to your hardware setup.

Todo:
- make menu items to start and stop webrepl and uftpd.
- Probably we need to be able to set the time that the centrifuge will be
  running when you click on start from preset
- And most probably we will need to change the displayed texts in the menu.
- To do calibration we would need to have a separate switch to power the motor.

It should not be too hard to design a complete new menu structure.

In the folder test you will find simple examples to explore how the library
classes work.

                 _________________
                 |  | |_| |_| |  |
                 |  | |       |  |
             rst-|               |-tx
              a0-|    esp8266    |-rx
              d0-|               |-d1-----scl(display)
   switch-----d5-|               |-d2-----sda(display)
rotaryClk-----d6-|               |-d3-----esc(motor)
rotaryDat-----d7-|               |-d4
              d8-|               |-gnd
             3v3-|               |-5v
                 -|              |
                  |______________|

