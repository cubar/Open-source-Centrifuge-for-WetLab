from ssd1306 import SSD1306_I2C
from machine import I2C, Pin
from menu import Menu
from rotary import Rotary
from switch import Switch

menuCfg = {
  'home': [['time', 'time()', 'time'],
           ['abc' , 'abc()' , 'home'],
           ['tst1', 'print("tst1")', 'tst2'],
           ['tst2', 'print("tst2")', 'tst3'],
           ['tst3', 'print("tst3")', 'tst4'],
           ['tst4', 'print("tst4")', 'tst5'],],
  'tst5': [['tst5', 'print("tst5")', 'home'],
           ['tst6', 'print("tst6")', 'home'],
           ['tst7', 'print("tst7")', 'home'],
           ['tst8', 'print("tst8")', 'home'],
           ['tst9', 'print("tst9")', 'home'],
           ['tst0', 'print("tst0")', 'home'],
          ],
}
oled_iic = I2C(scl=Pin(5),sda=Pin(4))
oled=SSD1306_I2C(width=128, height=32, i2c=oled_iic, addr=0x3c)
oled.fill(0)
menu = Menu(menuCfg, oled, x=0, y=0, w=32, h=32) # font size = 8x8px
menu.initText(menuCfg['tst5'])
oled.show()
rotary = Rotary(33, 32);
switch = Switch(19)
switch.setClick(menu.click)

def down(): menu.down()
def up(): menu.up()
rotary.setIncr(down)
rotary.setDecr(up)
