from rotary import Rotary

rotary = Rotary(14,12)

def decr():
  print('"decr" has been called: rotary val=%d' % rotary.read())
def incr():
  print('"incr" has been called: rotary val=%d' % rotary.read())

rotary.setDecr(decr)
rotary.setIncr(incr)


class Motor:
  def decr(self, val=-1):
    print('motor decr')
  def incr(self, val=1):
    print('motor incr')

motor = Motor()
rotary.setAction(motor)
