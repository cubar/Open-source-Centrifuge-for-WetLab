import sys
from motor import Esc


escPin = 17  #  motor is connected to this gpio number
motor = Esc(escPin)

def run():
  while True:
    print("Notice that the displayed duty cycle lags behind 1 step.")
    print('type "f" to increase speed or "s" to slow down.')
    c = sys.stdin.read(1)
    if   c=='f':
      print('faster')
      s = 1
    elif c=='s':
      print('slower')
      s = -1
    else:
      continue
    motor.change(s)

run()
