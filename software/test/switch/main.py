from switch import Switch

switch = Switch(19)

def click():     print('fun click called')
def longClick(): print('fun longClick called')

switch.setClick(click)
switch.setLongClick(longClick)
