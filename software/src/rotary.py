from machine import Pin

class Rotary:
  def __init__(self, p1, p2, pos=0):
    self.p1 = Pin(p1, Pin.IN, Pin.PULL_UP) # rotary clock
    self.p2 = Pin(p2, Pin.IN, Pin.PULL_UP) # rotary direction
    self.prev = 0
    self.pos = pos
    self.p1.irq(trigger=Pin.IRQ_FALLING|Pin.IRQ_RISING, handler=self._update)
    self.decr   = None # function to be called on turn left
    self.incr   = None # function to be called on turn right
    self.action = None # class instance with decr and incr methods

  def _update(self, pin):
    prev, self.prev = self.prev, pin.value()
    if pin.value() == 0: return
    if prev==0:
      if self.p2.value():
        self.pos -= 1;
        if self.decr: self.decr()
        if self.action: self.action.decr()
      else:
        self.pos += 1
        if self.incr: self.incr()
        if self.action: self.action.incr()

  def read(self):
    return self.pos

  def setDecr(self, decr):
    self.decr = decr

  def setIncr(self, incr):
    self.incr = incr

  def setAction(self, obj):
    print('motor.setAction: %s' % obj)
    d = dir(obj)
    if 'decr' not in d:
      print('Rotary.setAction: obj must have "decr" method')
    if 'incr' not in d:
      print('Rotary.setAction: obj must have "incr" method')
    self.action = obj
