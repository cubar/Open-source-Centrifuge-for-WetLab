from machine import Pin, Timer

class Switch(Pin):
  """
  just supply it a pin number and action functions as a dictionary:
  """
  def __init__(self, pin_number, period1=250, period2=100):
    self.pinNumber, self.p1, self.p2 = pin_number, period1, period2
    self.pin = Pin(pin_number)
    super().__init__(pin_number, Pin.IN, Pin.PULL_UP)
    self.click = None
    self.longClick = None
    self.timer = Timer(0)
    self.pin.irq(trigger=Pin.IRQ_FALLING, handler=self.debounce)
    self.busy = False

  def setClick(self, click):
    self.click = click

  def setLongClick(self, longClick):
    self.longClick = longClick

  def idle(self, timer):
    """ switch back to idle mode """
    self.busy = False

  def debounce(self, pin):
    if self.busy: return  # ignore the interrupt when busy
    self.busy = True # switch to busy mode
    self.timer.init(mode=Timer.ONE_SHOT, period=self.p1, callback=self.on_pressed)

  def on_pressed(self, timer):
    if self.pin.value():
      self.clickType = 'click'
    else:
      self.clickType = 'longClick'
    self.timer.init(mode=Timer.ONE_SHOT, period=self.p2, callback=self.ending)

  def ending(self, timer):
    if self.clickType=='click':
      if self.click:
        self.click()
      else:
        print('Switch.click not set')
    elif self.clickType=='longClick':
      if self.longClick:
        self.longClick()
      else:
        print('Switch.longClick not set')
    else:
      print('Switch: ending=>type=', self.clickType)
    self.timer.init(mode=Timer.ONE_SHOT, period=self.p1, callback=self.idle)
