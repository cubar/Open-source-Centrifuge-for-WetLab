from machine import Pin, PWM
from micropython import const

_MIN_THROTTLE = 51
_MAX_THROTTLE = 103
_MAX_MIN = _MAX_THROTTLE - _MIN_THROTTLE

class Esc:
  """
  With a pwm freq of 50 the cycle width is 20ms
  for the ESC the minimum duty width is 1ms and max is 2ms, being
  minimal 5% and max 10% as a range from 0 to 1023 inclusive for the esp8266
  or esp32.
  This evaluates to duty cycles zeroThrottle=51 and fullThrottle=103.
  To calibrate the motor, give the esc full throttle and power it up,
  wait for 3 seconds (2 should be sufficient), then set zero throttle.
  The motor should beep 2 seconds after startup then beep another time
  signaling that calibration is done.
  """

  def __str__(self):
    return 'ESC(%s, preset=%d)' % (self.pwm, self._preset)

  def __init__(self, pin):
    freq = 50  # pwm freq for any ESC
    self.pwm = PWM(Pin(pin), freq=freq, duty=_MIN_THROTTLE)
    self._display = None  # function that displays throttle on a screen
    self._preset = 0
    self._throttle(0)
    self.displayMode = 'live'

  def display(self, speed=None):
    if self._display:
      self._display(self.__throttle if speed==None else speed)

  def setDisplayFunction(self, display):
    self._display= display

  def setDisplayMode(self, mode):
    print('display mode=%s' % mode)
    self.displayMode = mode

  def _throttle(self, throttle):
    print('_throttle => %d' % throttle)
    self.pwm.duty(throttle+_MIN_THROTTLE)
    self.__throttle = throttle
    self.display()
    print('running: %s' % self)

  def change(self, faster):
    throttle = self.__throttle+faster
    throttle = min(max(throttle, 0), _MAX_MIN)
    self._throttle(throttle)

  def speed(self, speed):
    throttle = speed
    throttle = min(max(throttle, 0), _MAX_MIN)
    self._throttle(throttle)

  def start(self):
    preset = self._preset
    if preset:
      self.speed(preset)

  def stop(self):
    print('stopping')
    self._throttle(0)

  def fullSpeed(self):
    print('fullThrottle')
    self._throttle(_MAX_MIN)

  def preset(self, speed):
    """ preset speed for use with the start method """
    speed = min(max(0, speed), _MAX_MIN)
    self._preset = speed
    self.display(speed)

  def incr(self, val=1):
    if self.displayMode=='preset':
      self.preset(self._preset+val)
    else:
      self.speed(self.__throttle+val)

  def decr(self):
    self.incr(-1)
