# MicroPython menu driver on ssd1306 interfaces
#Code By WMD 2019-4-16 21:18:28
from micropython import const

import ssd1306

class Menu:
  """ coordinates in pixel, default font is 8x8 pixel """
  def __init__(self, cfg, display, x, y, w, h):
    '''
    Init menu bar area in a screen.
    Arg display is the ssd1306 object
    X and y is the start point of the screen
    w and h set the width and height of the menu.
    Tips: You can create a menu area which not use all of the screen space,so
    that you can use remaining area to display other views.
    Warning: This area couldn't bigger than the screen display area
    '''
    self.cfg=cfg # i.e. { menu1:[[display,action,next], ...], ... menuN[[...],...]
    self.display=display
    self.hightlightnum=0
    self.menux, self.menuy, self.menuw, self.menuh = x,y,w,h
    self.menuoffset=0

  def initText(self, TextList, offset=0, hightlightnum=0):
    '''
    Init menuText in menu bar.
    this function will refresh menu area to new TextList
    arg TextList should be a list. The first element is the text to display, and
    the second element is arg offset is use to display middle parts of the menu.
    the command.When submenu is needed, the command should be 'initText(theSubTextList)'
    '''
    self.display.fill_rect(self.menux,self.menuy,self.menuw,self.menuh,0) #clear old menu
    self.menuoffset=offset #Save offset value
    self.nowlist=TextList
    for i in range(len(TextList)):
      if (i+1)*10>self.menuh: #Text is out of range
        break
      self.display.text(TextList[i+offset][0], self.menux+1, self.menuy+i*10+1, 1)
    self.moveHighLight(hightlightnum)
    self.display.show()

  def __str__(self):
    print('MENU(cur={} off={})'.format(self.hightlightnum, self.menuoffset))

  def moveHighLight(self, num):
    '''
    Select HightLight num
    The argument num is relative the manu bar, not equal to Selected element
    '''
    self.display.rect(self.menux, self.menuy+self.hightlightnum*10, self.menuw, 10, 0) #clear old rect
    self.hightlightnum=num
    self.display.rect(self.menux, self.menuy+self.hightlightnum*10, self.menuw, 10, 1) #set new rect
    self.display.show()

  def down(self):
    if(self.menuoffset+self.hightlightnum == len(self.nowlist)-1): ## equal to max value
      return
    if (self.hightlightnum+2)*10>self.menuh: #Text is out of range
      self.initText(self.nowlist, self.menuoffset+1, self.hightlightnum) #refresh the Text List
    else:
      self.moveHighLight(self.hightlightnum+1)
      self.display.show()

  def up(self):
    if(self.menuoffset+self.hightlightnum == 0): ## equal to min value
      return
    if self.hightlightnum==0: #Text is out of range
      self.initText(self.nowlist, self.menuoffset-1, self.hightlightnum) #refresh the Text List
    else:
      self.moveHighLight(self.hightlightnum-1)
      self.display.show()

  def click(self):
    menuItem = self.nowlist[self.menuoffset+self.hightlightnum]
    exe = menuItem[1]
    if exe:
      try:
        eval(exe)
      except:
        print('Could not execute: (%s)' % exe)
    next = menuItem[2]
    nextMenu = self.cfg.get(next)
    if nextMenu:
      self.initText(nextMenu)
