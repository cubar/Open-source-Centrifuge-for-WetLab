import os

def ls():
  return os.listdir()

def cat(file):
  f = open(file)
  print(f.read())

def rm(file):
  os.remove(file)
