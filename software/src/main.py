from ssd1306 import SSD1306_I2C
from machine import I2C, Pin, Timer
from menu import Menu
from rotary import Rotary
from switch import Switch
from motor import Esc
from time import sleep

sleep(1)
cfg = {
  'pins': { # on esp8266 and esp32 these pins are fairly interchangable
                    # output pins:
    'scl'     :  5, # scl op 8266: gpio05 (d1)
    'sda'     :  4, # sda op 8266: gpio04 (d2)
    'motor'   :  0, # pwm op 8266: gpio00 (d3)
                    # input pins
    'switch'  : 14, # op 8266: gpio14 (d5)
    'rotary2' : 12, # op 8266: gpio12 (d6)
    'rotary1' : 13, # op 8266: gpio13 (d7)
  },
  'oled': { # 128x32px with fixed 8x8px font
    'width'      : 128,
    'height'     : 32,
    'address'    : 0x3c,
    'speed-x-pos': 113,
  },
  'menu': {
#    name        displayed_name    action        next_menu
    'home':    [( 'preset speed', 'rotaryAction("preset")', 'start' ),
                ( 'calibrate'   , 'calibrate()'           , 'stop'  ),
                ( 'live speed'  , 'rotaryAction("live")'  , 'stop'  ),],
    'start':   [( 'start'       , 'startMotor()'          , 'stop'  ),],
    'stop':    [( 'stop'        , 'stopMotor()'           , 'home'  ),],
  }
}

def createDisplay():
  display_iic = I2C(
    scl=Pin(cfg['pins']['scl']),
    sda=Pin(cfg['pins']['sda'])
  )
  display = SSD1306_I2C(
    width = cfg['oled']['width'],
    height= cfg['oled']['height'],
    i2c   = display_iic,
    addr  = cfg['oled']['address'],
  )
  display.fill(0)
  return display

def createMenu():
  menu = Menu(cfg['menu'], display, x=0, y=0, w=96, h=32) # font size = 8x8px
  menu.initText(cfg['menu']['home'])
  menu.incr = menu.down
  menu.decr = menu.up
  return menu

def createMotor():
  motor = Esc(cfg['pins']['motor'])
  return motor

def createRotary():
  rotary = Rotary(cfg['pins']['rotary1'], cfg['pins']['rotary2']);
  return rotary

# Here are the main objects
display = createDisplay()
menu    = createMenu()
motor   = createMotor()
rotary  = createRotary()
switch  = Switch(cfg['pins']['switch'])

# the following funtions manipulate the main objects
def menuHome():
  menu.initText(cfg['menu']['home'])
  eval('init()')

def showSpeed(speed):
  x_pos = cfg['oled']['speed-x-pos']
  display.fill_rect(x_pos, 0, 16, 8, 0) # clear region
  display.text(str(speed), x_pos, 0)
  display.show()

def calibrate():
  motor.fullSpeed()

def startMotor():
  motor.start()

def stopMotor():
  motor.stop()
  menuHome()

def rotaryAction(mode):
  """ mode should be "live" or "preset" """
  motor.setDisplayMode(mode)
  motor.display()
  rotary.setAction(motor)

def init():
  motor.setDisplayFunction(showSpeed) # is called by the motor on speed change
  rotary.setAction(menu)
  switch.setClick(menu.click)
  switch.setLongClick(stopMotor)

init()
