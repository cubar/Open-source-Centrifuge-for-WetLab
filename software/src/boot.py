# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
import uos, machine
#uos.dupterm(None, 1) # disable REPL on UART(0)
import network
ap = network.WLAN(network.AP_IF)
ap.active(True)
ap.config(essid='centrifuge')
ap.config(authmode=3, password='cent1234')
import gc
import webrepl
webrepl.start()
gc.collect()
from unix import *
#import uftpd # works on esp32 but not on esp8266
